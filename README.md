##	Realtime Statistics Code challenge

This API calculate realtime statistics from the last 60 seconds.

A messaging/Queuing system - Kafka was also implemented to cater for high volumes of requests and ensure the requests can be processed in parallel through the concept of consumer-groups and fix to the EXACTLY ONCE CHALLENGE that Kafka provides.

- All endpoints are exposed on (**Port: 8080**).


######Application Requirements:
- Java 8
- Maven 3+
- Kafka (running on http://localhost:9092)

######Deploying and Running application:
> This should be done from the root directory after Kafka is started
```
~$ mvn clean package spring-boot:run
```
Once the service is up, the application can be accessed on port 8080.

				
##   Cheers! 

