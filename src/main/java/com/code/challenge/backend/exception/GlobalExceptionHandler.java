package com.code.challenge.backend.exception;

import javax.validation.ValidationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(ValidationException.class)
	public  ResponseEntity<Void> handleTimeConstraintException(ValidationException se) {
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		
	}
	
}
