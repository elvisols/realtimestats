package com.code.challenge.backend.controller;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.state.WindowStore;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

import com.code.challenge.backend.model.TickerWindow;
import com.code.challenge.backend.model.Transaction;
import com.code.challenge.backend.model.TransactionStats;
import com.code.challenge.backend.util.JsonDeserializer;
import com.code.challenge.backend.util.JsonSerializer;
import com.code.challenge.backend.util.TransactionsBinding;
import com.code.challenge.backend.util.WrapperSerde;

import lombok.extern.slf4j.Slf4j;

/**
 * This class listens and processes transaction event, on real-time, to generating the 
 * count,
 * sum,
 * avg,
 * min, 
 * max 
 * of transactions over time.
 * 
 * @author  Elvis
 * @version 1.0, 26/05/18
 */
@Slf4j
@Component
@EnableBinding(TransactionsBinding.class)
public class TransactionSink {

	@StreamListener
	public void processTransactionInEvents(@Input(TransactionsBinding.TRANSACTION_IN) KStream<String, Transaction> events) {
		// process and store transaction statistics for real time querying from the QueryableStoreRegistry
		// see SpecsController.stats
		events
			.groupByKey()
			.windowedBy(TimeWindows.of(TimeUnit.MILLISECONDS.toMillis(1)))
			.aggregate(
        		TransactionStats::new,
        		(key, transaction, trxstats) -> trxstats.add(transaction), 
    			Materialized.<String, TransactionStats, WindowStore<Bytes, byte[]>>as(TransactionsBinding.STATISTICS_AGGREGATE_STORE)
    			.withKeySerde(Serdes.String())
                .withValueSerde(new TransactionStatsSerde()))
			.toStream((key, value) -> new TickerWindow(key.key(), key.window().start()))
			.peek((key, value) -> log.info("Realtime Value received = {} {} @{}", key, value, Instant.now()));
		
		// process and push transaction statistics in a 60secs windowed interval, for real-time push notifications to client.
		KStream<TickerWindow, TransactionStats> trxs = events
				.groupByKey()
				.windowedBy(TimeWindows.of(TimeUnit.SECONDS.toMillis(60)))
				.aggregate(
						TransactionStats::new,
						(key, transaction, trxstats) -> trxstats.add(transaction), 
						Materialized.<String, TransactionStats, WindowStore<Bytes, byte[]>>as(TransactionsBinding.STATISTICS_60SECS_AGGREGATE_STORE)
						.withKeySerde(Serdes.String())
						.withValueSerde(new TransactionStatsSerde()))
				.toStream((key, value) -> new TickerWindow(key.key(), key.window().start()))
				.peek((key, value) -> log.info("60secs Value received = {} {} @{}", key, value, Instant.now()));
		trxs.to("transactions-output", Produced.with(new TickerWindowSerde(), new TransactionStatsSerde()));
	}

	static public final class TransactionSerde extends WrapperSerde<Transaction> {
        public TransactionSerde() {
            super(new JsonSerializer<Transaction>(), new JsonDeserializer<Transaction>(Transaction.class));
        }
    }

    static public final class TransactionStatsSerde extends WrapperSerde<TransactionStats> {
        public TransactionStatsSerde() {
            super(new JsonSerializer<TransactionStats>(), new JsonDeserializer<TransactionStats>(TransactionStats.class));
        }
    }

    static public final class TickerWindowSerde extends WrapperSerde<TickerWindow> {
        public TickerWindowSerde() {
            super(new JsonSerializer<TickerWindow>(), new JsonDeserializer<TickerWindow>(TickerWindow.class));
        }
    }
}
