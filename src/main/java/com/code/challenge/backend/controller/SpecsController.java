package com.code.challenge.backend.controller;

import java.time.Instant;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyWindowStore;
import org.apache.kafka.streams.state.WindowStoreIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.binder.kafka.streams.QueryableStoreRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.code.challenge.backend.model.Transaction;
import com.code.challenge.backend.model.TransactionStats;
import com.code.challenge.backend.util.TransactionsBinding;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

/**
 * This class exposes realtime statistics from the last 60 seconds
 * 
 * <pre>
 * 	Allowed methods: GET and POST
 * </pre>
 * @author elvis
 *
 */
@Slf4j
@RestController
public class SpecsController {

	private final MessageChannel tranxaction;
	
	@Autowired
	private QueryableStoreRegistry registry;
	
	public SpecsController(TransactionsBinding binding) {
		this.tranxaction = binding.transactionOut();
	}
	
	/**
	 * @param void
	 * @method GET
	 * @return result  as TransactionStats object of count, sum, min, max, and avg of transactions in a 60 seconds window
	 * @throws InterruptedException
	 */
	@GetMapping(value="/statistics", produces=MediaType.APPLICATION_JSON_VALUE)
	@Synchronized
	public ResponseEntity<TransactionStats> stats() throws InterruptedException {
		TransactionStats transactionStats = new TransactionStats();
		ReadOnlyWindowStore<String, TransactionStats> queryableStoreType = 
				this.registry.getQueryableStoreType(TransactionsBinding.STATISTICS_AGGREGATE_STORE, 
				QueryableStoreTypes.windowStore());
		WindowStoreIterator<TransactionStats> all = queryableStoreType.fetch("testit", Instant.now().minusSeconds(60).toEpochMilli(), System.currentTimeMillis());
		while(all.hasNext()) {
			KeyValue<Long, TransactionStats> val = all.next();
			if (transactionStats.getSum() == 0) transactionStats.setMin(val.value.getSum());
			transactionStats.setSum(transactionStats.getSum() + val.value.getSum());
			transactionStats.setCount(transactionStats.getCount() + val.value.getCount());
			transactionStats.setMin(Math.min(val.value.getMin(), transactionStats.getMin()));
			transactionStats.setMax(Math.max(val.value.getMax(), transactionStats.getMax()));
		}
		transactionStats.setAvg(transactionStats.getSum() / transactionStats.getCount());
		return new ResponseEntity<TransactionStats>(transactionStats, HttpStatus.OK);
	}
	
	/**
	 * This method is used to post new transactions that happened in the past 60 seconds in json
	 * 
	 * @param <b>amount</b> transaction amount (e.g 12.3)
	 * @param <b>timestamp</b> transaction event time (epoch in millis e.g 1478192204000). This should not be more than 60 seconds old
	 * @param errors this stores any error e.g TimeConstraint exception
	 * @return void
	 */
	@PostMapping(value="/transactions", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> postTransaction(@Valid @RequestBody Transaction transaction, Errors errors) {
		log.info("Posting transaction...{}", transaction);
		if (errors.hasErrors()) {
			log.error("Error in transaction body detected...{}", errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(", ")));
			throw new ValidationException(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(", ")));
		}
		Message<Transaction> trx = MessageBuilder
				.withPayload(transaction)
				.setHeader(KafkaHeaders.MESSAGE_KEY, "testit".getBytes())
				.build();
		try {
			this.tranxaction.send(trx);
			log.info("message sent!...");
		} catch(Exception e) {
			log.error(e.getMessage());
		}
		
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
}

