package com.code.challenge.backend.model;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import com.code.challenge.backend.util.TimeConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
	
	@NotNull(message="Amount parameter missing!")
	@Digits(integer=11, fraction=2, message="Amount format exception! - Enter an 11 digit or 2 decimal places value")
	private Double amount;
	
	
	@NotNull(message="Timestamp parameter missing!")
	@TimeConstraint
	private Long timestamp;
	
}
