package com.code.challenge.backend.model;

public class TickerWindow {
    String ticker;
    long timestamp;

    public TickerWindow(String ticker, long timestamp) {
        this.ticker = ticker;
        this.timestamp = timestamp;
    }

	@Override
	public String toString() {
		return "TickerWindow [ticker=" + ticker + ", timestamp=" + timestamp + "]";
	}
}
