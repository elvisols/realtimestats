package com.code.challenge.backend.model;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TransactionStats {

	private long count;
	private double sum;
	private double avg;
	private double max;
	private double min;
	private long time;
	private String timestamp;
	
	public TransactionStats add(Transaction trx) {
		
		if (count == 0) this.min = trx.getAmount();
        
        this.count++;
        
        this.sum += trx.getAmount();
        
        this.min = Math.min(this.min, trx.getAmount());
        
        this.max = Math.max(this.max, trx.getAmount());
        
        this.time = trx.getTimestamp();
        
        this.avg = this.sum / this.count;
        
        this.timestamp = Instant.now().toString();
        
        return this;
	}
	
	@Override
	public String toString() {
		return "TransactionStats [count=" + count + ", sum=" + sum + ", avg=" + avg + ", max=" + max
				+ ", min=" + min + ", timestamp=" + timestamp + ", time=" + time + "]";
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	@JsonIgnore
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@JsonIgnore
	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}

