package com.code.challenge.backend.util;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

import com.code.challenge.backend.model.Transaction;

public interface TransactionsBinding {

	String STATISTICS_AGGREGATE_STORE = "stat-agg";
	String STATISTICS_60SECS_AGGREGATE_STORE = "stat-60secs-agg";
	String TRANSACTION_IN = "transactionin";
	String TRANSACTION_OUT = "transactionout";

	@Input(TRANSACTION_IN)
	KStream<String, Transaction> transactionIn();
	
	@Output(TRANSACTION_OUT)
	MessageChannel transactionOut();
	
}
