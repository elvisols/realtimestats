package com.code.challenge.backend.util;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TimeValidator implements ConstraintValidator<TimeConstraint, Long> {

  @Override
  public void initialize(TimeConstraint time) {
  }

  @Override
  public boolean isValid(Long timeField, ConstraintValidatorContext cxt) {
	  Date now = new Date();
	  return (((now.getTime() - timeField) / 1000) > 0 && ((now.getTime() - timeField) / 1000) <= 60) ? true : false;
  }

}
