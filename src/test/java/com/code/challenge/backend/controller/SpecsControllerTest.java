package com.code.challenge.backend.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.aop.TargetSource;
import org.springframework.aop.framework.Advised;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.binder.kafka.streams.QueryableStoreRegistry;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.http.MediaType;
import org.springframework.integration.channel.QueueChannel;
import static org.springframework.integration.test.matcher.PayloadMatcher.hasPayload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;

import org.apache.kafka.streams.state.ReadOnlyWindowStore;
import org.apache.kafka.streams.state.WindowStoreIterator;
import org.hamcrest.Matchers;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.code.challenge.backend.model.Transaction;
import com.code.challenge.backend.model.TransactionStats;

//@RunWith(SpringRunner.class)
public class SpecsControllerTest {
/*
 * TODO
	private MockMvc mockMvc;

	@InjectMocks
	@Qualifier("transactionout")
	MessageChannel tranxaction;
	
	
	@Mock
	ReadOnlyWindowStore<String, TransactionStats> queryableStoreType;
	
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(tranxaction).build();
	}
	
	@Test
	public void postTransactionTest() throws Exception {
		Transaction transaction = new Transaction(10.0, Instant.now().minusSeconds(5).toEpochMilli());
		Message<Transaction> trx = MessageBuilder
				.withPayload(transaction)
				.setHeader(KafkaHeaders.MESSAGE_KEY, "testit".getBytes())
				.build();
		when(tranxaction.send(trx)).thenReturn(true);
		mockMvc.perform(post("/transactions")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
		verify(tranxaction.send(trx));
	}
*/

}
