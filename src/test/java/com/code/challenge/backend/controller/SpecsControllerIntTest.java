package com.code.challenge.backend.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.integration.test.matcher.PayloadMatcher.hasPayload;

import java.time.Instant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.code.challenge.backend.model.Transaction;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpecsControllerIntTest {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	@Qualifier("transactionout")
	MessageChannel tranxaction;
	
	@Test
	public void postTransactionNoContentTest() {
		Transaction transaction = new Transaction(10.0, 1527498610000L); // post a transaction older than 60 secs
		ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/transactions", transaction, Void.class);
		
		assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
	}
	
	@Test
	@DirtiesContext
	public void postTransactionTest() {
		Transaction transaction = new Transaction(10.0, Instant.now().minusSeconds(5).toEpochMilli());
		ResponseEntity<Void> responseEntity = restTemplate.postForEntity("/transactions", transaction, Void.class);
		
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	}
	
	@Test
	@DirtiesContext
	public void postTransactionUnitTest() {
		Transaction transaction = new Transaction(10.0, 1527498610000L);
		Message<Transaction> message = MessageBuilder
				.withPayload(transaction)
				.setHeader(KafkaHeaders.MESSAGE_KEY, "testit".getBytes())
				.build();
		boolean sent = tranxaction.send(message);
		assertThat(message, hasPayload(transaction));
		assertTrue(sent);
		
	}

}
